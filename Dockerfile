FROM python:3

RUN pip install paho-mqtt

ADD hello_world.py /

CMD [ "python", "./hello_world.py" ]
