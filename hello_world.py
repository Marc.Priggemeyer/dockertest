import sys
import paho.mqtt.client as paho

broker="18.194.15.143"
port=1883

def on_publish(client,userdata,result):             #create function for callback
  print("published \n")
  pass

client= paho.Client("dzboxpublisher")
client.on_publish = on_publish
client.connect(broker,port)

ret = client.publish("dzbox/test/hello_world", "Hello from DZBox.")
ret.wait_for_publish()


